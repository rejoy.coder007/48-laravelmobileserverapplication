import 'package:flutter/material.dart';




import 'package:aa_flutter_app/Config.dart';
import 'dart:convert';




import 'package:http/http.dart' as http;
import 'package:random_string/random_string.dart';

import 'ItemFromBookGridDetails.dart';

class fullbook extends StatefulWidget {
  @override
  _fullProductState createState() => _fullProductState();
}


class ProductItem
{



  String cartid ;

  ProductItem(this.cartid, this.name, this.picture, this.old_price, this.price,
      this.product_id, this.size, this.details, this.description);

  String name ;




  String picture ;
  String old_price ;
  String price ;
  String product_id ;


  String size  ;
  String details  ;
  String description  ;

}
class _fullProductState extends State<fullbook> {

  List <ProductItem> product_array = new List();

  Future<String> GetFullCartsById() async
  {
    List data;


    print("ORGADDRESS :: " + Config.url_show_book_by_id);
    final response = await http.post(Uri.parse(Config.url_show_book_by_id), body: {

      "user_id": Config.Id.toString()
    }, headers: {"Accept": "application/json"});




    setState(()
    {
      var resBody = json.decode(response.body.toString());
      data = resBody["results"];

      product_array.clear();


      for( var i = 0 ; i<data[0].length/Config.NUMBER_RANDOM_CART_HOME_ITEM_SIZE; i++ ) {





        String name = data[0]["cloth_name"+i.toString()];
        String picture = data[0]["image_url"+i.toString()];
        String old_price = data[0]["old_price"+i.toString()].toString();
        String price = data[0]["new_price"+i.toString()].toString();
        String product_id = data[0]["product_id"+i.toString()].toString();
        String cartId = data[0]["cartId"+i.toString()].toString();

        String size = data[0]["size"+i.toString()].toString();
        String details = data[0]["details"+i.toString()].toString();
        String description = data[0]["description"+i.toString()].toString();




        //  print(Config.MyIpAddress+data[0][img]);
        product_array.add( new ProductItem(cartId,name,picture,old_price,price,product_id,size,details,description));


      }










    });






    return "Success!";

  }

  @override
  initState() {
    super.initState();


    GetFullCartsById();



  }




  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: product_array.length,
        gridDelegate:
        new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
        itemBuilder: (BuildContext context, int index) {
          return Single_prod(
            cartID: product_array[index].cartid,
            prod_name: product_array[index].name,
            prod_pricture: Config.MyIpAddress+product_array[index].picture,
            prod_old_price: product_array[index].old_price,
            prod_price: product_array[index].price,
            prod_id: product_array[index].product_id,
            size: product_array[index].size,
            details: product_array[index].details,
            description: product_array[index].description,



          );
        });
  }
}

class Single_prod extends StatelessWidget {
  final cartID;
  final prod_name;
  final prod_pricture;
  final prod_old_price;
  final prod_price;
  final prod_id;
  final size;
  final details;
  final description;

  Single_prod({
    this.cartID,
    this.prod_name,
    this.prod_pricture,
    this.prod_old_price,
    this.prod_price,this.prod_id,this.size,this.details,this.description
  });

  @override
  Widget build(BuildContext context) {


    return Card(
      child: Hero(
          tag: randomString(10),
          child: Material(
            child: InkWell(
                    child: GridTile(
                child: Container(
                    color: Colors.white,
                    // child: Image.asset(widget.prod_pricture)
                    //  Image.network(prod_pricture, fit: BoxFit.cover)
                    child: Image.network(prod_pricture, fit: BoxFit.contain)
                ),
                footer: new Container(
                  color: Colors.white70,
                  child: ListTile(
                    leading: new Text(
                      prod_name,
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 16.00,color: Colors.blueGrey,),
                    ),
                    title: new Row(
                      children: <Widget>[
                        Expanded(
                          child: new Text("\$${prod_old_price}",
                            style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.w800,
                                decoration
                                    :TextDecoration.lineThrough),
                          ),


                        ),
                        Expanded(
                          child: new Text("\$${prod_price}",
                            style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w800,
                            ),),
                        ),




                      ],
                    ),


                  ),
                ),
              ),


            ),


          )),

    );


  }
}




