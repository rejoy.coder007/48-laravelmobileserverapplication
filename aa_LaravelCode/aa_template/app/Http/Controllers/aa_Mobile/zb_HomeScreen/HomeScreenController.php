<?php

namespace App\Http\Controllers\aa_Mobile\zb_HomeScreen;

use App\aa_ProductApparel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeScreenController extends Controller
{
    /// Get Random Images from
    public function get_random_carousel_apparel_images(Request $request)
    {
        //

      //  return response()->json($request->Icount);



        $random_carousel = aa_ProductApparel::inRandomOrder()->take((int)$request->Icount)->get();




        $val = array();
        $data = array();

        //$val["ok_1"]=$random_carousel[0];
        // $val["ok_2"]=$random_carousel[0];

        for ($i = 0; $i < sizeof($random_carousel); $i++) {


            $val["image".$i] =$random_carousel[$i]->image_url;
        }













        $data["results"][]=$val;


        return response()->json($data);
    }


    public function productVarieties(Request $request)
    {
        $ProductEntries =null;

        switch($request->type){

            case 0:
                $ProductEntries =\App\aa_ProductApparel::all()->random($request->Pcount);
                break;
            case 1:
                $ProductEntries = \App\aa_ProductApparel::all();
                break;
            case 2:
                $ProductEntries = \App\aa_ProductApparel:: where('cloth_name', 'T shirt')->get();

                break;


            case 3:
                $ProductEntries = \App\aa_ProductApparel:: where('cloth_name', 'frock')->get();


                break;
            case 4:
                $ProductEntries = \App\aa_ProductApparel:: where('cloth_name', 'Pants')->get();


                break;


            case 5:
                $ProductEntries = \App\aa_ProductApparel:: where('cloth_name', 'Formal')->get();

                break;

            case 6:
                $ProductEntries = \App\aa_ProductApparel:: where('cloth_name', 'sari')->get();

                break;



            default:
                break;
        }








        $val = array();
        $data = array();

        //$val["ok_1"]=$random_carousel[0];
        // $val["ok_2"]=$random_carousel[0];

        for ($i = 0; $i < sizeof($ProductEntries); $i++) {

            $val["product_id".$i] =$ProductEntries[$i]->id;
            $val["cloth_name".$i] =$ProductEntries[$i]->cloth_name;
            $val["size".$i] =$ProductEntries[$i]->size;
            $val["details".$i] =$ProductEntries[$i]->details;

            $val["image_url".$i] =$ProductEntries[$i]->image_url;
            $val["new_price".$i] =$ProductEntries[$i]->new_price;

            $val["old_price".$i] =$ProductEntries[$i]->old_price;
            $val["description".$i] =$ProductEntries[$i]->description;






        }















        $data["results"][]=$val;


        return response()->json($data);


    }


}
