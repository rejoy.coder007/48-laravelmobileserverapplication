<?php

namespace App\Http\Controllers\aa_Mobile\zd_AdminPage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function  admin_db( Request $request)
    {



        return view('AdminLogin');
       // return response()->json("hello");

        // return response()->json("hello");


        /*

        $user =null;
        $user = \App\User::where('email', $request->email)->first();

        $val = array();
        $data = array();

        if ($user != null)
        {
            // validate credentials
            if ( !password_verify( $request->password, $user->password))
            {


                $val["id"]=$user->id;
                $val["OK"]=1;
                $val["admin"]=10000;
                $val["msg"]="Password Wrong";
                $val["email"]="Password Wrong";
                $val["name"]="Password Wrong";


            }
            else
            {

                $val["id"]=$user->id;
                $val["OK"]=2;
                $val["admin"]=$user->admin;
                $val["msg"]="Success";

                $val["email"]=$user->email;
                $val["name"]=$user->name;






            }



        }
        else
        {
            $val["id"]=$user->id;
            $val["OK"]=3;
            $val["admin"]=10000;

            $val["msg"]="Username Wrong";
            $val["email"]="Password Wrong";
            $val["name"]="Password Wrong";

        }




        $data["results"][]=$val;


        return response()->json($data);
        */



    }
    //


    public function  admin_db_post( Request $request)
    {



       // return view('AdminLogin');
        // return response()->json("hello");

       // $data= $request->name;
       // return $data;
      //  dd($data);


        //return response()->json( $request->all());

        // return response()->json("hello");




        $user =null;
        $user = \App\User::where('email', $request->email)->first();

       // return response()->json($user->id);

        $val = array();
        $data = array();

        if ($user != null  )
        {
            // validate credentials
            if ( !password_verify( $request->password, $user->password))
            {


                $val["id"]=$user->id;
                $val["OK"]=1;
                $val["admin"]=10000;
                $val["msg"]="Password Wrong";
                $val["email"]="Password Wrong";
                $val["name"]="Password Wrong";
                return redirect('wrongpassword');


            }
            else
            {

                $val["id"]=$user->id;
                $val["OK"]=2;
                $val["admin"]=$user->admin;
                $val["msg"]="Success";

                $val["email"]=$user->email;
                $val["name"]=$user->name;

               if($user->admin ==1)

                return redirect('adminUpload');
               else
                   return redirect('wrongpassword');






            }



        }
        else
        {
            $val["id"]=$user->id;
            $val["OK"]=3;
            $val["admin"]=10000;

            $val["msg"]="Username Wrong";
            $val["email"]="Password Wrong";
            $val["name"]="Password Wrong";

            return redirect('wrongpassword');

        }




        $data["results"][]=$val;


         return response()->json($data);




    }


    public function  admin_wrong_password( Request $request)
    {


        return view('WrongPassword');
    }
    public function  admin_db_upload_get( Request $request)
    {



        return view('AdminUpload');
        // return response()->json("hello");

        // return response()->json("hello");


        /*

        $user =null;
        $user = \App\User::where('email', $request->email)->first();

        $val = array();
        $data = array();

        if ($user != null)
        {
            // validate credentials
            if ( !password_verify( $request->password, $user->password))
            {


                $val["id"]=$user->id;
                $val["OK"]=1;
                $val["admin"]=10000;
                $val["msg"]="Password Wrong";
                $val["email"]="Password Wrong";
                $val["name"]="Password Wrong";


            }
            else
            {

                $val["id"]=$user->id;
                $val["OK"]=2;
                $val["admin"]=$user->admin;
                $val["msg"]="Success";

                $val["email"]=$user->email;
                $val["name"]=$user->name;






            }



        }
        else
        {
            $val["id"]=$user->id;
            $val["OK"]=3;
            $val["admin"]=10000;

            $val["msg"]="Username Wrong";
            $val["email"]="Password Wrong";
            $val["name"]="Password Wrong";

        }




        $data["results"][]=$val;


        return response()->json($data);
        */



    }


    public function  admin_upload_data( Request $request)
    {


        // return response()->json($request->email);
        $cover = $request->file('bookcover');
        $extension = $cover->getClientOriginalExtension();
        Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));

/*
        'cloth_name' =>$item_name,
        'size' => $size[$rand_size],
        'details' => $faker->word,
        "image_url"=>$img_url,
        'new_price' => $new_price,
        'old_price' => $old_price,
        'description' => $faker->text,
    */

        $ProductItem = new \App\aa_ProductApparel();
        $ProductItem->cloth_name = $request->cloth_name;
        $ProductItem->size = $request->size;
        $ProductItem->details = $request->details;
        $ProductItem->image_url =  "/uploads/".$cover->getFilename().'.'.$extension;
        $ProductItem->new_price =  $request->new_price;
        $ProductItem->old_price =  $request->old_price;
        $ProductItem->description =  $request->description;
        $ProductItem->save();



        /*
        $book = new Book();
        $book->name = $request->name;
        $book->author = $request->author;
        $book->mime = $cover->getClientMimeType();
        $book->original_filename = $cover->getClientOriginalName();
        $book->filename = $cover->getFilename().'.'.$extension;
        $book->save();
        */

        return redirect()->route('gotoupload');

    }


}
