<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<body>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-heading">
            <h2 class="text-center">Admin Upload information</h2>
        </div>
        <hr />
        {{--
        <div class="modal-body">
            <form action="/adminUpload" role="form" method="post">
                <div class="form-group">
                    <div class="input-group">
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
							</span>
                        <input type="email" class="form-control" name="email" placeholder="email" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-lock"></span>
							</span>
                        <input type="password" name="password" class="form-control" placeholder="Password" />

                    </div>

                </div>

                <div class="form-group">
                    <div class="input-group">
                        <label for="author">Cover:</label>
                        <input type="file" name="bookcover" class="form-control" placeholder="Upload Image" />

                    </div>

                </div>





                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success btn-lg text-center">Login</button>

                </div>

            </form>
        </div>

        --}}
        <form method="post" action="/adminUpload" enctype="multipart/form-data">

            <!--

             'cloth_name' =>$item_name,
        'size' => $size[$rand_size],
        'details' => $faker->word,
        "image_url"=>$img_url,
        'new_price' => $new_price,
        'old_price' => $old_price,
        'description' => $faker->text,
            -->


            <div class="form-group">
                <label for="cloth_name">Cloth Name</label>
                <input type="text" class="form-control" name="cloth_name"/>
            </div>


            <div class="form-group">
                <label for="size">Size</label>
                <input type="text" class="form-control" name="size"/>
            </div>


            <div class="form-group">
                <label for="details">Details</label>
                <input type="text" class="form-control" name="details"/>
            </div>


            <div class="form-group">
                <label for="new_price">New Price</label>
                <input type="text" class="form-control" name="new_price"/>
            </div>


            <div class="form-group">
                <label for="old_price">Old Price</label>
                <input type="text" class="form-control" name="old_price"/>
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" name="description"/>
            </div>




            <div class="form-group">
                <label for="bookcover">Cover:</label>
                <input type="file" class="form-control" name="bookcover"/>
            </div>


            <div class="form-group text-center">
                <button type="submit" class="btn btn-success btn-lg text-center">Upload new product</button>

            </div>


        </form>
    </div>
</div>




<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-heading">
           <hr>
        </div>
        <hr />
        <div class="modal-body">


            <div class="form-group text-center">
                <button type="submit" class="btn btn-success btn-lg text-center"> <a href="/adminLogin">
                        LogOut
                    </a></button>

            </div>

        </div>
    </div>
</div>




</body>
</html>
