<?php

$unique_No=0;

$factory->define(App\User::class, function (Faker\Generator $faker) {

    $row = array();
    switch ( aa_UserTable::$count)
    {
        case 0:

            $row = [
                'name' => "rambo",
                'email' => "a@a.com",
                'admin' => 1,
                'address' =>$faker->address,

                'password' => bcrypt("a"),

            ];

        break;


        case 1:

            $row = [
                'name' => "remo",
                'email' => "b@b.com",
                'admin' => 0,
                'address' =>$faker->address,

                'password' => bcrypt("b"),

            ];

        break;

        default:

            $row = [
                'name' => $faker->name,
                'email' => $faker->safeEmail,
                'address' =>$faker->address,
                'admin' => 0,

                'password' => bcrypt($faker->password),

            ];

        break;
    }


    aa_UserTable::$count++;

    return $row;








});


$factory->define(App\aa_ProductApparel::class, function (Faker\Generator $faker) {


    $input = array("T shirt", "Pants", "Formal", "frock","sari");

    $size = array("XXL", "XL");




    $rand_size= array_rand($size, 1);
    $old_price = random_int(2000,5000);

    $new_price =  random_int($old_price-100,$old_price-10);


    $img_url =  "/images/Apparel/product_".aa_ApparelProductSeeder::$count.".jpeg";



    $item_name = $input[aa_ApparelProductSeeder::$count];

    aa_ApparelProductSeeder::$count++;
    return [

        'cloth_name' =>$item_name,
        'size' => $size[$rand_size],
        'details' => $faker->word,
        "image_url"=>$img_url,
        'new_price' => $new_price,
        'old_price' => $old_price,
        'description' => $faker->text,
    ];
});
$factory->define(App\aa_Cart_App::class, function (Faker\Generator $faker) {
    return [
        'new_price' => $faker->randomNumber(),
        'old_price' => $faker->randomNumber(),
        /*
        'description' => $faker->text,
        'item_id' =>random_int(0,10),*/
    ];
});

$factory->define(App\aa_Booking_App::class, function (Faker\Generator $faker) {
    return [
        'new_price' => $faker->randomNumber(),
        'description' => $faker->text,
        'item_id' => random_int(0,10),
    ];
});

